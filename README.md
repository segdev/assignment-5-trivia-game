# trivia-game-app

## Goal of this assignment

Goal of this assignment is to build an online trivia game as Single Page Application using Vue (version 2.x)

## Specs of the assignment

Game has following screens:

1. The start screen
2. The load screen
3. The question screen
4. The results screen

More about components and their relations you can find from [component tree](https://gitlab.com/segdev/assignment-5-trivia-game/-/blob/main/Component%20Tree.pdf)

Also more details about assignment you can find from [assignment pdf](https://gitlab.com/segdev/assignment-5-trivia-game/-/blob/main/Assignment_3_Vue_Trivia_Game.pdf) 

## APIs

[Trivia API](https://opentdb.com/api_config.php)

API for storing players and their highscores [Heroku](https://segdev-noroff-assignment-api.herokuapp.com/trivia)

## Heroku

Link to the game on [heroku](https://segdev-trivia-game.herokuapp.com/)
