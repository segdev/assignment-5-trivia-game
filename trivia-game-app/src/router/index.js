import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'StartPage',
        component: () => import(/* webpackChunkName: "start" */ '../components/StartScreen/StartScreen.vue')
    },
    {
        path: '/questions',
        name: 'QuestionPage',
        component: () => import(/* webpackChunkName: "questions" */ '../components/QuestionsScreen/QuestionsScreen.vue')
    },
    {
        path: '/results',
        name: 'ResultsPage',
        component: () => import(/* webpackChunkName: "questions" */ '../components/ResultsScreen/ResultsScreen.vue')
    },
    {
        path: '/load',
        name: 'LoadScreen',
        component: () => import(/* webpackChunkName: "load" */ '../components/StartScreen/LoadScreen.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router