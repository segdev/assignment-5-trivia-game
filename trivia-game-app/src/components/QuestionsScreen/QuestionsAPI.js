export const QuestionsAPI = {
    // Fetches all categories
    getCategories() {
        return fetch("https://opentdb.com/api_category.php")
            .then(response => response.json())
            .then(data => data.trivia_categories)
            .catch(e => console.log('Error occured in getCategories', e.message))
    },
    // Gets new token from Trivia API
    getToken() {
        return fetch("https://opentdb.com/api_token.php?command=request")
            .then(response => response.json())
            .then(data => data.token)
            .catch(e => console.log('Error occured in getToken', e.message))
    },
    // Fetches questions with given token, category, difficulty and amount of questions
    getQuestions(token, category, difficulty, amoutOfQuestions) {
        return fetch(`https://opentdb.com/api.php?amount=${amoutOfQuestions}&category=${category}&difficulty=${difficulty}&token=${token}`)
            .then(response => response.json())
            .then(data => {
                if(data.response_code === 0){
                    return data.results
                }
                else {
                    return data.response_code
                }
            })
            .catch(e => console.log('Error occured in getQuestions', e.message))
    }
}