export const UsersAPI = {
    // Checks if user is already in db, returns user or empty array
    checkForUser(username) {
        return fetch(`https://segdev-noroff-assignment-api.herokuapp.com/trivia?username=${username}`)
            .then(response => response.json())
            .then(data => data)
            .catch(e => console.log('Error occured', e.message))
    },
    // Creates new user in db, returns user
    createUser(username) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'X-API-Key': 'tFGpEKnUC9LrynUbesK4wcTmkScm0b93J33t6ouhSZCGo4V8YbfF8BovJruIZzut',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                highscore: 0
            })
        }

        return fetch("https://segdev-noroff-assignment-api.herokuapp.com/trivia", requestOptions)
            .then(response => response.json())
            .then(user => user)
            .catch(e => console.log('Error occured', e.message))
    },
    // Updates new highscore in db
    updateHighscore(highscore, id) {
        const requestOptions = {
            method: 'PATCH',
            headers: {
                'X-API-Key': 'tFGpEKnUC9LrynUbesK4wcTmkScm0b93J33t6ouhSZCGo4V8YbfF8BovJruIZzut',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                highscore: highscore
            })
        }

        return fetch(`https://segdev-noroff-assignment-api.herokuapp.com/trivia/${id}`, requestOptions)
            .then(response => response.json())
            .then(user => user)
            .catch(e => console.log('Error occured', e.message))
    }
}