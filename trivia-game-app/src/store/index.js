import Vue from 'vue'
import Vuex from 'vuex'
import { UsersAPI } from '@/components/StartScreen/UsersAPI'
import { QuestionsAPI } from '@/components/QuestionsScreen/QuestionsAPI'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        username: '',
        profile: {},
        newHighscore: '',
        numbOfQuestions: '',
        category: 'none',
        difficulty: 'easy',
        allCategories: [],
        questions: [],
        rightAnswers: [],
        playersAnswers: [],
        token: '',
        error: ''
    },
    mutations: {
        setUsername: (state, payload) => {
            state.username = payload
        },
        setProfile: (state, payload) => {
            state.profile = payload
        },
        setNewHighscore: (state, payload) => {
            state.newHighscore = payload
        },
        setNumbOfQuestions: (state, payload) => {
            state.numbOfQuestions = payload
        },
        setCategory: (state, payload) => {
            state.category = payload
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setAllCategories: (state, payload) => {
            state.allCategories = payload
        },
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setRightAnswers: (state, payload) => {
            state.rightAnswers = payload
        },
        setPlayersAnswers: (state, payload) => {
            state.playersAnswers = payload
        },
        setToken: (state, payload) => {
            state.token = payload
        },
        setError: (state, payload) => {
            state.error = payload
        }
    },
    actions: {
        async checkUser({ state, commit }) {
            try {
                // Checks if user is already in db
                let user = await UsersAPI.checkForUser(state.username)

                if(user.length ){

                    commit( 'setProfile', user )
                } else {
                    // If not, adds new user to the db
                    const user = await UsersAPI.createUser(state.username)
                    if(user){
                        commit( 'setProfile', user )
                    } else {
                        commit( 'setError', 'Could not register the new user')
                    }
                }
            } catch (e) {
                commit( 'setError', e.message)
            }
        },
        async updateHighscore({ state, commit }) {
            const { profile } = state
            // Checks first if users new highscore is bigger than the one in db
            if(state.newHighscore > profile[0].highscore){
                try {
                    const user = await UsersAPI.updateHighscore(state.newHighscore, profile[0].id)
                    if(user){
                        commit( 'setProfile', user )
                    } else {
                        commit( 'setError', 'Could not update the highscore')
                    }
                } catch (e) {
                    commit( 'setError', e.message)
                }
            }
        },
        async fetchCatergories({ commit }) {
            try {
                const categories = await QuestionsAPI.getCategories()

                if(categories){
                    commit( 'setAllCategories', categories )
                } else {
                    commit( 'setError', 'Could not fetch categories' )
                }
            } catch (e) {
                commit( 'setError', e.message )
            }
        },
        async getToken({ commit }) {
            try {
                const token = await QuestionsAPI.getToken()

                if(token){
                    commit( 'setToken', token )
                }
            } catch (e) {
                commit( 'setError', e.message)
            }
        },
        async getQuestions({ state, commit, dispatch }) {
            try {
                const questions = await QuestionsAPI.getQuestions(state.token, state.category, state.difficulty, state.numbOfQuestions)
                if(!Array.isArray(questions)){
                    switch(questions){
                        case 1:
                            commit( 'setError', 1 )
                            console.log('In questions error 1: Could not return results. The API doesnt have enough questions for your query.')
                            break;
                        case 2:
                            commit( 'setError', 2 )
                            console.log('In questions error 2: Contains an invalid parameter. Arguements passed in arent valid.')
                            break;
                        case 3:
                            commit( 'setError', 3 )
                            console.log('In questions error 3: Session Token does not exist.')
                            dispatch('getToken')
                            break;
                        case 4:
                            commit( 'setError', 4 )
                            console.log('In questions error 4: Session Token has returned all possible questions for the specified query. Resetting the Token is necessary.')
                            break;
                        default:
                            commit( 'setError', 5 )
                            console.log('In questions default error: Some error occured while retrieving questions')
                            break;
                    } 
                }
                else {
                    commit( 'setQuestions', questions )
            }
            } catch (e) {
                commit( 'setError', e.message )
            }
        }
    }
})